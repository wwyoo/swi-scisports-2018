import matplotlib.pyplot as plt
import numpy as np
from matplotlib import animation
from matplotlib.animation import FuncAnimation

ani = None


def colour_maker(colour, fade):
    colours = {"red": (1, 0, 0), "green": (0, 1, 0), "blue": (0, 0, 1)}
    if colour in colours:
        rgb_colour = np.array(colours[colour])
        rgb_white = np.array([1, 1, 1])
        vector = rgb_white-rgb_colour
        return tuple((rgb_colour + vector * fade))
    else:
        return tuple((0.5, 0.5, 0.5))


def plot_static(data, field_l, field_w, colours=None, filename=None):
    if len(data.shape) == 2:
        data = [data]
    N = len(data)
    if not colours:
        colours = ["black" for i in range(N)]
        shades = ["gray" for i in range(N)]
    else:
        shades = [colour_maker(clr, 0.3) for clr in colours]
    # settings for maintaining the ratio and showing the whole field
    min_l, max_l = -field_l/2, field_l/2
    min_w, max_w = -field_w/2, field_w/2
    fig = plt.figure(figsize=(10, 7))
    ax = fig.add_subplot(111)
    ax.set_xlim(min_l, max_l)
    ax.set_ylim(min_w, max_w)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_facecolor((170/255, 1, 153/255))
    plt.gca().set_aspect('equal', adjustable='box')
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    for i, d in enumerate(data):
        plt.plot([p[0] for p in d], [p[1] for p in d],
                    c=shades[i], linewidth=3)
        plt.plot([d[-1][0]], [d[-1][1]], c=colours[i], marker='o', markersize=6)
    if filename:
        plt.savefig(filename)
    return plt


def plot_dynamic(data, field_l, field_w, colours=None, mspf=25, tail=36, filename=None):
    data = [data] if len(data.shape) == 2 else data
    N = len(data)
    if not colours:
        colours = ["black" for i in range(N)]
        shades = ["gray" for i in range(N)]
    else:
        shades = [colour_maker(clr, 0.3) for clr in colours]
    fig = plt.figure(figsize=(10, 7))
    ax = fig.add_subplot(111)
    xdata, ydata = [[] for i in range(N)], [[] for i in range(N)]
    ln = [plt.plot([], [], c=shades[i//2], linewidth=3, animated=True)[0] if i%2==0 else
          plt.plot([], [], c=colours[i//2], marker='o', markersize=6, animated=True)[0] for i in range(2*N)]

    min_l, max_l = -field_l / 2, field_l / 2
    min_w, max_w = -field_w / 2, field_w / 2

    def init():
        ax.set_xlim(min_l, max_l)
        ax.set_ylim(min_w, max_w)
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_facecolor((170 / 255, 1, 153 / 255))
        plt.gca().set_aspect('equal', adjustable='box')
        return ln

    def update(k):
        for i, d in enumerate(data):
            xdata[i].append(d[k][0])
            ydata[i].append(d[k][1])
            ln[2*i].set_data(xdata[i][-tail:], ydata[i][-tail:])
            ln[2*i+1].set_data([d[k][0]], [d[k][1]])
        return ln

    # 'global ani' is needed, otherwise it gets garbage collected and the plot will be empty :(
    global ani
    ani = FuncAnimation(fig, update, init_func=init, frames=len(data[0]),
                        repeat=False, blit=True, interval=mspf)
    if filename:
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=30, metadata=dict(artist='Me'), bitrate=10000)
        ani.save(filename, writer=writer)

    return plt

if __name__ == "__main__":
    # print("at this point you should have the data files in the 'ROOT/data' folder")

    # plot_static([[100,100],[200,200],[100,400]], 10500, 6800).show()
    # plot_dynamic([[100,100],[200,200],[100,400]], 10500, 6800).show()
    p = [i for i in range(10)]
    plt.scatter(p, p, color=[colour_maker("red", i/10) for i in range(10)])
    plt.show()

