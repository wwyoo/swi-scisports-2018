# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 00:44:38 2018

@author: Anatoliy
"""

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pandas as pd
import time

class AnimatedScatter(object):
    """An animated scatter plot using matplotlib.animations.FuncAnimation."""
    def __init__(self, data, start_time = 0):
        self.time = start_time
        self.data = data
        self.init_data = self.data.iloc[self.data.index.get_level_values('T') == start_time]
        # Setup the figure and axes...
        self.fig, self.ax = plt.subplots()
        # Then setup FuncAnimation.
        self.ani = animation.FuncAnimation(self.fig, self.update, interval=5, 
                                           init_func=self.setup_plot, blit=True)

    def setup_plot(self):
        """Initial drawing of the scatter plot."""
        x, y = self.init_data.X, self.init_data.Y
        s = np.repeat(1, x.__len__())
        c = np.repeat(1, x.__len__())
        self.scat = self.ax.scatter(x, y, c=c, s=s, animated=True)
        self.ax.axis([self.data.X.min(), self.data.X.max(), self.data.Y.min(), self.data.Y.max()])

        # For FuncAnimation's sake, we need to return the artist we'll be using
        # Note that it expects a sequence of artists, thus the trailing comma.
        return self.scat,

    def update(self, i):
        """Update the scatter plot."""
        self.cur_data = self.data.iloc[self.data.index.get_level_values('T') == self.time]
        self.time += 100
        # Set x and y data..
        x, y = self.cur_data.X, self.cur_data.Y
        s = np.repeat(1, x.__len__())*20
        c = np.repeat(1, x.__len__())

        self.scat = self.ax.scatter(x, y, c=c, s=s, animated=True)

        time.sleep(0.01)
        # We need to return the updated artist for FuncAnimation to draw..
        # Note that it expects a sequence of artists, thus the trailing comma.
        return self.scat,

    def show(self):
        plt.show()

def generate_BM_data(timesteps = 100, players = 22):
    time_steps = [i*100 for i in range(0,timesteps)]
    player_ids= range(players)
    index = pd.MultiIndex.from_product([player_ids, time_steps], names=['player_id', 'T'])
    data = np.concatenate([np.random.rand(2)+(np.random.rand(timesteps,2)-0.5).cumsum(axis=0)/100 for i in player_ids])
    data = pd.DataFrame(data = data, index=index, columns = ['X','Y'] )
    return data

def generate_NP_data(timesteps = 1000, players = 22):
    time_steps = [i*100 for i in range(0,timesteps)]
    player_ids= range(players)
    index = pd.MultiIndex.from_product([player_ids, time_steps], names=['player_id', 'T'])
    #data = np.concatenate([np.random.rand(2)+np.ma.average((np.random.rand(timesteps,2)-0.5).cumsum(axis=0), weights = [[np.exp(-0.1*(10-i)),np.exp(-0.1*(10-i))]] for i in range(10)], axis=1)/100 for i in player_ids])
    #data = np.concatenate([np.random.rand(2)+(np.random.rand(timesteps,2)-0.5).cumsum(axis=0) for i in range(10)], axis=1)/100 for i in player_ids]
    data = pd.DataFrame(data = data, index=index, columns = ['X','Y'] )
    return data
    
if __name__ == '__main__':
    default_data = generate_BM_data(timesteps = 10**4)
    #default_data = generate_NP_data(timesteps = 10**3)
    #match_df.index.get_level_values('player_id').unique()

    a = AnimatedScatter(data = default_data)
    a.show()