from __future__ import print_function

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from keras.layers import Input, Dense, Lambda
from keras.models import Model
from keras import backend as K
from keras import metrics
import time as tm
# some_file.py
#import sys
#sys.path.append('/E:\SciSports\swi-scisports-2018\src\preprocess')
#sys.path.append('/E:\SciSports\swi-scisports-2018')
'''from src.preprocess.prepare_data import Match
match = Match(77317)
match.build()
'''
from src.preprocess.prepare_data import Matches
m = Matches()

ts = tm.time()
batch_size = 100
original_dim = 2000
latent_dim =30
intermediate_dim = 400
epochs = 100
epsilon_std = 1.0


x = Input(shape=(original_dim,))
h = Dense(intermediate_dim, activation='relu')(x)
z_mean = Dense(latent_dim)(h)
z_log_var = Dense(latent_dim)(h)


def sampling(args):
    z_mean, z_log_var = args
    epsilon = K.random_normal(shape=(K.shape(z_mean)[0], latent_dim), mean=0.,
                              stddev=epsilon_std)
    return z_mean + K.exp(z_log_var / 2) * epsilon

# note that "output_shape" isn't necessary with the TensorFlow backend
z = Lambda(sampling, output_shape=(latent_dim,))([z_mean, z_log_var])

# we instantiate these layers separately so as to reuse them later
decoder_h = Dense(intermediate_dim, activation='relu')
decoder_mean = Dense(original_dim, activation='sigmoid')
h_decoded = decoder_h(z)
x_decoded_mean = decoder_mean(h_decoded)

# instantiate VAE model
vae = Model(x, x_decoded_mean)

# Compute VAE loss
xent_loss = original_dim * metrics.mean_squared_error(x, x_decoded_mean)
kl_loss = - 0.5 * K.sum(1 + z_log_var - K.square(z_mean) - K.exp(z_log_var), axis=-1)
vae_loss = K.mean(xent_loss)

vae.add_loss(vae_loss)
vae.compile(optimizer='rmsprop')
vae.summary()

batch = m.next_batch_player_ball_from_all(10000, length=500)
batch_min = batch.min(axis=(0, 1), keepdims=True)
batch_max = batch.max(axis=(0, 1), keepdims=True)
x_train = (batch - batch_min)/(batch_max - batch_min) #normalization
x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))

vae.fit(x_train,
        shuffle=True,
        epochs=epochs,
        batch_size=batch_size)

# build a model to project inputs on the latent space
encoder = Model(x, z_mean)

# build a digit generator that can sample from the learned distribution
decoder_input = Input(shape=(latent_dim,))
_h_decoded = decoder_h(decoder_input)
_x_decoded_mean = decoder_mean(_h_decoded)
generator = Model(decoder_input, _x_decoded_mean)
print(tm.time()-ts)

# plot trajectory
b = m.next_batch_player_ball_from_all(1, length=500)
b_min = b.min(axis=(0, 1), keepdims=True)
b_max = b.max(axis=(0, 1), keepdims=True)
x_test = (b - batch_min)/(batch_max - batch_min) #normalization
x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
z = encoder.predict(x_test)
x_d = generator.predict(z)
x_dr = x_d.reshape(1,2,500, 2)
x_dr = batch_min + (batch_max-batch_min)*x_dr
from src.visual.plot_stuff import plot_dynamic, plot_static
plot_static(x_dr[0],10500,6800).show()
plot_static(b[0],10500,6800).show()
plot_dynamic(x_dr[0],10500,6800).show()
plot_dynamic(b[0],10500,6800).show()
